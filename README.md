#For create proxmox rule into fail2ban need just run commands from the sudo user or root

echo " " >> /etc/fail2ban/jail.conf

echo "[proxmox]" >> /etc/fail2ban/jail.conf

echo "enabled = true" >> /etc/fail2ban/jail.conf

echo "port = https,http,8006" >> /etc/fail2ban/jail.conf

echo "filter = proxmox" >> /etc/fail2ban/jail.conf

echo "logpath = /var/log/daemon.log" >> /etc/fail2ban/jail.conf

echo "maxretry = 3" >> /etc/fail2ban/jail.conf

echo "# 1 hour" >> /etc/fail2ban/jail.conf

echo "bantime = 3600" >>  /etc/fail2ban/jail.conf

touch /etc/fail2ban/filter.d/proxmox.conf

echo "[Definition]" >> /etc/fail2ban/filter.d/proxmox.conf

echo "failregex = pvedaemon[.authentication failure; rhost= user=. msg=.*" >> /etc/fail2ban/filter.d/proxmox.conf

echo "ignoreregex =" >> /etc/fail2ban/filter.d/proxmox.conf

systemctl restart fail2ban
